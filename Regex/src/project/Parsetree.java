package project;

public class Parsetree {
	private Parsetree left, right;
	private char value;
	private static int j=0;
	
	/**
	 * Constructs a parsetree with the childen pright and pleft. 
	 * The value is of the new parsetree is pvalue.
	 * @param pleft left child of the new parsetree
	 * @param pright right child of the new parsetree
	 * @param pvalue value of the new parsetree
	 */	
	public Parsetree( Parsetree pleft, Parsetree pright, char pvalue){ // Erzeugt einen Suchbaum mit vorgegebenen Kindern und Inahlt
		value = pvalue;
		left = pleft;
		right = pright;
	}
	
	
	/**
	 * Constructs a new parsetree with the value a.
	 * The left and right child of the new parsetree are null.
	 * @param a
	 */
	public Parsetree(char a){
		value = a;
	}
	
	/*
	 * Puts out the completely parsetree.
	 */
	public void treeOutput(){
		treeOutput(0);
	}
	
//	public void setJ(int i){
//		j = i;
//	}
	
	/**
	 * Puts out the completely parsetree indented by i times "  ".
	 * @param i  the parsetree is indented by i times "  ".
	 */
	private void treeOutput(int i){
		for (int j=i; j>0; j--) System.out.print("  ");
		System.out.println(value);
		if( right != null ) right.treeOutput(i+1);		
		if( left != null ) left.treeOutput(i+1);
	}
	
	/**
	 * Constructs a new parsetree based on the regular expression p.
	 * @param p The regular expression for the new parsetree
	 * @return New parsetree based on regular expression p.
	 */
	public static Parsetree parsetree(String p){
//		System.out.print(p + " wurde konvertiert zu ");
		p = regularExchange(p);
//		System.out.println(p);
		Parsetree res = parse(p);
		j=0;
//		System.out.println("Exit");
		return res;
	}
	
//	static String regularExchange(String p){
//		for(int i=0; i<p.length(); i++){
//			if(p.charAt(i)=='+'){
//				// Falls das (i-1)-te Zeichen keine Klammer ist ersetze das + an der i-ten Stelle durch den Ausdruck "p[i-1]*"
//				if(')' != p.charAt(i-1)) {	// Prüfe ob nur ein einzelnes Zeichen vor dem + steht
//					String left = p.substring(0,i-1);
//					char mid = p.charAt(i-1);
//					if(i+1 < p.length()){	// Prüfe ob das + am Ende des Ausdrucks stand 
//						String right = p.substring(i+1, p.length());
//						p = left + mid + "(" + mid + "*)" + right;						
//					} else 	p = left + mid + "(" + mid + "*)";
//					i += 3;
//				} else{
//					// Zähler für offene und geschlossene Klammern läuft von links nach rechts
//					// durch Intervall bis genauso viele Klammern geschlossen wurden wie auch geöffnet
//					int count = -1;	
//					// leftInt gibt am Ende die Stelle an, an der die Klammer geöffnet wurde
//					int leftInt = i;
//					for(int j=i-2; count != 0; j--){	
//						if(p.charAt(j) == ')') count --;  
//						if(p.charAt(j) == '(') count ++;
//						leftInt --;	
//					} 
//					String left = p.substring(0, leftInt-1) + p.substring(leftInt,i-1);
//					// mid wird die Wiederholung des Ausdrucks der von + betroffen war 
//					String mid = p.substring(leftInt-1, i-1) + "*)"; // enhält die Anfangsklammer
//					if(i+1 < p.length()){	// Prüfe ob das + am Ende des Ausdrucks stand 
//						String right = p.substring(i+1, p.length());
//						p = left + mid + "(" + mid + "*)" + right;
//					}
//					p = left + mid + "(" + mid + "*)";
//					i+= mid.length()+2;
//				}
//			}
//			if(p.charAt(i)=='?'){
//				if(')' != p.charAt(i-1)){	// Prüfe ob nur ein einzelenes Zeichen vor dem ? steht
//					String left = p.substring(0, i);
//					if(i+1 < p.length()){	// Prüfe ob das ? am Ende des Ausdrucks stand 						
//						String right = p.substring(i+1, p.length());
//						p = "(" + left + "|?)" + right; 
//					} else {
//						p = "(" + left + "|?)";
//						
//					}
//					i += 4; 
//				} else {
//					int count = -1;
//					int leftInt = i;
//					for(int j=i-2; count != 0; j--){
//						if(p.charAt(j) == ')') count --;  
//						if(p.charAt(j) == '(') count ++;
//						leftInt --;	
//					}
//					String left = p.substring(0, leftInt-1);
//					String mid = p.substring(leftInt, i-1);
//					if(i+1 < p.length()){
//						String right = p.substring(i+1, p.length());
//						p = left + "(" + mid + "|?)" + right;
//					} else p = left + "(" + mid + "|?)";
//					i += 2;
//				}
//				
//			}
//		}
//		return p;
//	}

	/**
	 * Converts the regular expressions p. 
	 * Exchanges q+ with q(q*) for q regular expression. 
	 * Exchanges q? with (q|?) for q regular expression.
	 * After the exchange ? symbols epsilon in the regular expression.
	 * @param p The regular expression, which should be converted
	 * @return The converted regular expression
	 */
	private static String regularExchange(String p){
		for(int i=0; i<p.length(); i++){
			if(p.charAt(i)=='+'){
				// Falls das (i-1)-te Zeichen keine Klammer ist ersetze das + an der i-ten Stelle durch den Ausdruck "p[i-1]*"
					// Zähler für offene und geschlossene Klammern läuft von links nach rechts
					// durch Intervall bis genauso viele Klammern geschlossen wurden wie auch geöffnet
					int count = 0;	
					// leftInt gibt am Ende die Stelle an, an der die Klammer geöffnet wurde
					int leftInt = i;
					for(int j=i-1; count != 1; j--){	
						if(p.charAt(j) == ')') count --;  
						if(p.charAt(j) == '(') count ++;
						leftInt --;	
					} 
//					System.out.println("leftInt = " + leftInt);
					String left = p.substring(0, leftInt);
//					System.out.println("left = " + left);
					// mid wird die Wiederholung des Ausdrucks der von + betroffen war 
					String mid = p.substring(leftInt+1, i); // enhält die Anfangsklammer
//					System.out.println("mid = " + mid);
					if(i+1 < p.length()){	// Prüfe ob das + am Ende des Ausdrucks stand 
						String right = p.substring(i+2, p.length());
//						System.out.println("right = " + right);
						p = left + mid + "(" + mid + "*)" + right;
					} else p = left + mid + "(" + mid + "*)";
					i+= mid.length();
//					System.out.println("p = " + p);				
			}
			if(p.charAt(i)=='?'){
					int count = 0;
					int leftInt = i;
					for(int j=i-1; count != 1; j--){
						if(p.charAt(j) == ')') count --;  
						if(p.charAt(j) == '(') count ++;
						leftInt --;	
					}
					String left = p.substring(0, leftInt);
//					System.out.println("left = " + left);
					String mid = p.substring(leftInt+1, i);
//					System.out.println("mid = " + mid);
					if(i+1 < p.length()){
						String right = p.substring(i+2, p.length());
//						System.out.println("right = " + right);
						p = left + "(" + mid + "|?)" + right;
					} else p = left + "(" + mid + "|?)";
//					System.out.println("p = " + p);	
					i += 2;
				}							
		}
		return p;
	}
		
	/**
	 * Contructs a parsetree based on the parse algorithm of the skript and based on the regular expression p.
	 * @param p Regular expression for the parsetree
	 * @return	Parsetree based on the regular expression p
	 */
	static Parsetree parse(String p){
		Parsetree left = null;
		while (j<p.length()){
//			System.out.println(j);
			if (alphabetElement(p.charAt(j))){
				//System.out.print(j + " alpha      ");
				Parsetree right = new Parsetree(p.charAt(j));
				j++;
				if ( left != null ) {					
					left = new Parsetree(left, right, '.');
				} else left = right; 
			} else if (p.charAt(j) == '('){
				//System.out.print(j + " (      ");
				j++;
				Parsetree right = parse(p);
				j++;
				if( left != null ) {
					left = new Parsetree(left, right, '.');
				} else left = right; 
			} else if (p.charAt(j) == '|'){
			//	System.out.print(j + " |      ");
				j++;
				Parsetree right = parse(p);
				left = new Parsetree(left, right, '|');
			} else if(p.charAt(j) == '*'){
			//	System.out.print(j + " *      ");
				j++;
				left = new Parsetree(left, null, '*');
			} else if (p.charAt(j) == ')'){
			//	System.out.print(j + " )      ");
				return left;
			} 
		}
		return left;
	}
	
	/**
	 * Checks if a is element of { *, |, (, )} or not
	 * @param a character, which should be checked
	 * @return return true if a isn't element of { *, |, (, )} 
	 */
	private static boolean alphabetElement(char a){ //Gibt zurück ob das Zeichen aus dem Alphabet ist. Gibt für ( ) * | falsch zurück.
		 char[] alphabet = {'*','|','(',')'};
		 for(int i=0; i<alphabet.length; i++){	// Durchläuft das Sonderzeichen Alphabet und guckt ob a enthalten ist
			 if(a==alphabet[i]) return false;
		 }
		 return true;
	 }

	public Parsetree getLeft() {
		return left;
	}

	public void setLeft(Parsetree left) {
		this.left = left;
	}

	public Parsetree getRight() {
		return right;
	}

	public void setRight(Parsetree right) {
		this.right = right;
	}

	public char getValue() {
		return value;
	}

	public void setValue(char value) {
		this.value = value;
	}
}
