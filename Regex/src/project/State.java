package project;

import java.util.*;

public class State {
	private State left;
	private State right;
	private char leftvalue;
	private char rightvalue;
	private boolean isEpsilon;
	
	/**
	 * Constructs an epsilon state
	 */
	
	public State(){
		this.isEpsilon=true;
	}
	
	public boolean getisEpsilon(){
		return isEpsilon;
	}
	public State getLeft() {
		return left;
	}
	public void setLeft(State left) {
		this.left = left;
	}
	public State getRight() {
		return right;
	}
	public void setRight(State right) {
		this.right = right;
	}
	public char getLeftvalue() {
		return leftvalue;
	}
	public void setLeftvalue(char leftvalue) {
		this.leftvalue = leftvalue;
		this.isEpsilon=false;
	}
	public char getRightvalue() {
		return rightvalue;
	}
	public void setRightvalue(char rightvalue) {
		this.rightvalue = rightvalue;
		this.isEpsilon=false;
	}
	
	/**
	 * Does a search in an Automaton for the String t via the Set-Based-Simulation
	 * @param t the string you want to find
	 * @param start The start state of the automaton
	 * @param end The end state of the automaton
	 * @return The first position and the longest substring from the string
	 */
	
	
	public static RegexMatchResult bSearch(String t, State start, State end){
		int n=t.length();
		int j;
		char key;
		boolean boolbuff=false;	
		HashSet<State> S = new HashSet<State>();
		RegexMatchResult buffregex=new RegexMatchResult(-1,"");
		for(int i=0;i<n;i++){ //gives you all states to which you can get by an epsilon
			S.clear();
			
			S.addAll(epsilonend(start,end));
			j=i-1;
			
			if(S.contains(end) && S.contains(start)){
				S.remove(start); S.remove(end);
			}
			
			
		while(!S.isEmpty()){
			

			State[] buff2 = new State[S.size()];
			S.toArray(buff2);
			S.clear();
			for(int k=0;k<buff2.length;k++){
				S.addAll(epsilonend(buff2[k],end));
			}
			
			if(S.contains(end)){ //if true then the substring is accepted
				String buff="";
				for(int k=i;k<=j;k++){	
					buff=buff+t.charAt(k);
					}
				buffregex=new RegexMatchResult(i,buff);
				if(S.size()==1){ //if there is only the end state return the regex
					return buffregex;
					}	
				}
			j++;
			if(j==n){//if true then go to the next i
				if(S.size()>0){
					return buffregex;
					}
				S.clear();
				}else{
					key=t.charAt(j);
					State[] buff = new State[S.size()];
					S.toArray(buff);
					if(S.contains(end)){
						boolbuff=true;
					} else {
						boolbuff=false;
					}
					S.clear();
					for(int k=0;k<buff.length;k++){	
						S.addAll(SearchNode(buff[k],key,end));
					}
					if(S.contains(end) && S.size()==1 &&boolbuff){
						return buffregex;
					}	
				}
			}
		}
		return buffregex;
	}
	
	
	/**
	 * search in a state to which states you can get with the key
	 * @param s the state you start in
	 * @param key the char for you transition
	 * @param end the end state of the automaton
	 * @return all states to which you come from s with the key
	 */
	
	private static HashSet<State> SearchNode(State s, char key, State end){
		
		HashSet<State> S = new HashSet<State>();
		if(s==end){
			S.add(s); return S;
		}	
		if(s.getisEpsilon()){
			if(s.getLeft()!=null){
				S.addAll(SearchNode(s.getLeft(),key,end));
			}
			if(s.getRight()!=null){
				S.addAll(SearchNode(s.getRight(),key,end));
			}
		}
		if(s.getLeftvalue()==key){
			S.add(s.getLeft());
		}
		if(s.getRightvalue()==key){
			S.add(s.getRight());
		}
		return S;
	}
	
	/**
	 * gives you back all states you come from your start state with an epsilon transition
	 * @param s the start state
	 * @param end the end state of the automaton
	 * @return all states you come from your start state with an epsilon transition 
	 */
	
	private static HashSet<State> epsilonend(State s, State end){
		
		HashSet<State> S = new HashSet<State>();
		if(s.getisEpsilon()){
			if(s.getLeft()!=null){
				S.addAll(epsilonend(s.getLeft(),end));
			}
			if(s.getRight()!=null){
				S.addAll(epsilonend(s.getRight(),end));
			}
			
		} 
		S.add(s);
		return S;		
	}
	
	/**
	 * Searches and finds all States, that can be reached from s via key.
	 * @param s the state you start in
	 * @param key the char for the transition
	 * @param end the end state of the automaton
	 * @return all states to which you come from s with the key
	 */	
	private static HashSet<State> SearchNode2(State s, char key, State end){
		
		HashSet<State> S = new HashSet<State>();
		
		if(s.getisEpsilon()){
			if(s.getLeft()!=null){
				S.addAll(SearchNode2(s.getLeft(),key,end));
			}
			if(s.getRight()!=null){
				S.addAll(SearchNode2(s.getRight(),key,end));
			}
			
		}

		if(s.getLeftvalue()==key){
			S.add(s.getLeft());
			S.addAll(epsilonend(s.getLeft(),end));
		}
		
		if(s.getRightvalue()==key){
			S.add(s.getRight());
			S.addAll(epsilonend(s.getRight(),end));
		}
		
		return S;
		
		
		
		
	}
/**
 * Does a search in an Automaton to find the first, longest word in the String text,
 * which is accepted by the Automaton, via the Path-Based-Simulation.
 * @param text the String being searched
 * @param start The start state of the automaton
 * @param end The end state of the automaton
 * @return The first position and the longest matched substring from the string
 */

	public static RegexMatchResult itSearch(String text, State start, State end){
		RegexMatchResult res=new RegexMatchResult(-1,"");
		int n=text.length();
		for (int i=0;i<n;i++){
			Stack<StateAndValue> stack= new Stack<StateAndValue>();
			stack.push(new StateAndValue(start,i));
			while (! stack.empty()){
				StateAndValue tmp=stack.pop();
				if(tmp.getState()==end && tmp.getValue()-i>res.getMatchedString().length()){
					res=new RegexMatchResult(i,text.substring(i, tmp.getValue()));
				}
				if (tmp.getValue()<text.length()){
					HashSet<State> S = SearchNode2(tmp.getState(),text.charAt(tmp.getValue()),end);
					if(!S.isEmpty()){
						State[] buff2 = new State[S.size()];			
						S.toArray(buff2);
						for (int j=0;j<buff2.length;j++){
									stack.push(new StateAndValue(buff2[j],tmp.getValue()+1));
						}
					}
				}

			}
			if (res.getMatchedString()!=""){
				return res;
			}
		}
		return res;
	}
	
	
}
