package project;
import java.util.Arrays;

public class RegexMatcher {
	public static RegexMatchResult matchSetBased (String regex, String text){
		
		
		Parsetree buffParsetree = Parsetree.parsetree(regex);
		Automaton testAutomaton = new Automaton(buffParsetree);		
		RegexMatchResult result = State.bSearch(text, testAutomaton.getStart(), testAutomaton.getEnd());
		
		return result;
	}
	public static RegexMatchResult matchPathBased (String regex, String text){
		
		Parsetree buffParsetree = Parsetree.parsetree(regex);
		Automaton testAutomaton = new Automaton(buffParsetree);		
		RegexMatchResult result = State.itSearch(text, testAutomaton.getStart(), testAutomaton.getEnd());
		
		return result;
	}
	
	/**
	 * Checks whether an expression is regular or not.
	 * @param regex the expression to be checked.
	 * @param alphabet the Alphabet.
	 * @return true, if the expression is regular, false if not.
	 */
	
	public static boolean isRegularExpression(String regex, char[] alphabet){
		if(regex.length() > 0) {
			// Pr�fe ob nur Zeichen aus dem Alphabet und (, ), |, * vorkommen
			char[] symbols = {'*', '|', '(', ')'};
			char[] legalChar = Arrays.copyOf(alphabet, alphabet.length+symbols.length);
			System.arraycopy(symbols, 0, legalChar, alphabet.length, symbols.length);
			for(int i=0; i<regex.length(); i++){
				boolean legal = false;
				for(int j=0; j < legalChar.length; j++){	// Pr�fe ob das Zeichen an der i-ten Stelle im Array legalChar vorkommt
					if (regex.charAt(i)==legalChar[j]) legal = true;	
				}
				if(!legal) return legal; // return false falls ein falsches Zeichen enthalten ist
			}
			
			// Pr�fe auf unzul�ssige Startzeichen
			char[] illegalChar = {'*', ')', ')'};	// Unzul�ssige Startzeichen
			for(int i=0; i<illegalChar.length; i++){
				if(regex.charAt(0) == illegalChar[i]) return false;
			}
			
			// Pr�fe ob alle Klammern die ge�ffnet werden auch geschlossen werden
			// und ob zu viele Klammern ge�ffnet oder geschlossen werden
			int count=0;
			for(int i=0;i<regex.length();i++){
				if (regex.charAt(i) == '(') count = count+1;			
				if (regex.charAt(i) == ')') count = count-1;
				if ( count < 0 ) return false;
			}
			if ( count != 0 ) return false;
			
			// Pr�fe auf unzul�ssige Zeichenfolgen wie:
			// ()   (|   |)   (*   |*   )(
			String[] illegal = {"()", "(|", "|)", "(*", "|*", ")("}; // Unzul�ssige Zeichenfolgen
			for(int i=0;i<regex.length()-1;i++){
				for(int j=0; j<illegal.length; j++){
					if(regex.substring(i, i+1).equals(illegal[j])) return false;
				}
			}
		}		
		return true;	// return true wenn kein Fehler aufgetreten ist
	}
}
