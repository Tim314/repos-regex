package project;

public class StateAndValue {
	State state;
	int value;
	public StateAndValue(State s, int v){
		this.value=v;
		this.state=s;
	}
	public State getState() {
		return state;
	}
	public int getValue() {
		return value;
	}
}
