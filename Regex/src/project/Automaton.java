package project;


public class Automaton {

	private State start;
	private State end;
	
	/**
	 * Recursively constructs an automata from a Parstree with given Start- and End-States.
	 * @param p A Parsetree.
	 * @param start Starting State.
	 * @param end Ending State.
	 */
	private void translate (Parsetree p,State start,State end){
	if (p!=null){
			if (p.getValue()=='.'){   // wie vergleicht man chas?
				State tmp=new State();
				translate (p.getLeft(),start,tmp);
				translate (p.getRight(),tmp,end);
			}
			else if (p.getValue()=='|'){
				State tmp=new State();
				start.setLeft(tmp);
				State leftend = new State();
				leftend.setLeft(end);
				tmp=new State();
				start.setRight(tmp);
				State rightend=new State();
				rightend.setLeft(end);
				translate(p.getLeft(),start.getLeft(),leftend);
				translate(p.getRight(),start.getRight(),rightend);	
			}
			else if(p.getValue()=='*'){
				start.setRight(end);
				start.setLeft(new State());
				State leftend=new State();
				leftend.setRight(start);
				leftend.setLeft(end);
				translate (p.getLeft(),start.getLeft(),leftend);
			}
			else if(p.getValue()=='?'){
				start.setLeft(end);
			}
			else{
				start.setLeft(end);
				start.setLeftvalue(p.getValue());
			}
		}
	}
	
	public State getStart() {
		return start;
	}

	public State getEnd() {
		return end;
	}
	
	/**
	 * Constructs an automata from a Parsetree.
	 *
	 * @param  p Parsetree, that gets transformend into an automata.
	 */
	public Automaton(Parsetree p){
		this.start=new State();
		this.end=new State();
		translate(p,start,end);
	}
	
}
