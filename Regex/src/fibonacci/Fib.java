package fibonacci;

public class Fib {
	public static int rekfib(int n){

		if (n<0){
			return 0;
		} else if (n==0){
			return 1;
		} else if (n==1) {
			return 1;
		}

		return rekfib((n-2))+rekfib((n-1));

	} 

	public static int itfib(int n){

		if (n<=0){
			return 1;
		}

		int[] erg = new int[n+1];

		erg[0]=1;
		erg[1]=1;


		for(int i=2; i<=n; i++){

			erg[i]=erg[i-2]+erg[i-1];

		}

		return erg[n];

	}
}
