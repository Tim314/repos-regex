package fibonacci;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class FibonacciTest {
	
	@Test
	  public void rekfikTest() {
	    
	    int sum = Fib.rekfib(7);
	    assertEquals(21, sum);
	    assertTrue(21== sum);
	  }
	
	@Test
	  public void itfibTest() {
	    
	    int sum = Fib.itfib(10);
	    assertEquals(89, sum);
	  }
	
	@Test
	  public void rekfibItfibTest() {
	    
	    int sum1 = Fib.itfib(15);
	    int sum2 = Fib.rekfib(15);
	    assertEquals(sum1, sum2);
	  }

}
